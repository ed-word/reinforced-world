# Reinforced World

A reinforcement based emergency response system that optimises lives saved and time taken to save lives.

---
Steps:  

- sudo add-apt-repository ppa:kirillshkrogalev/ffmpeg-next
- sudo apt update  
- sudo apt install ffmpeg  
- pip3 install -r requirements.txt  
- python3 Server/manage.py runserver  
---

![](Images/flow.jpeg)

---
 
